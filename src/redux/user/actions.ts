/** Save application info */
export const SetAppInfo = "user/setAppInfo";

/** Save app settings */
export const SetSettings = "user/setSettings";

/** Save loaded notifications */
export const SetNotifications = "user/setNotifications";

/** Toggle sort order for notifications */
export const ToggleNotificationsSortOrder = "user/toggleNotificationsSort";

/** Save loaded data about measurements */
export const SetMeasurementsWithoutDiff = "user/setMeasurementsWithoutDiff";
